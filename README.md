# MC Diaspora

Mike's coding challenge submission for Best Buy.
Submitted on Wednesday, November 18th, 2020.

## Build the Maven project
Clone this project and then run **mvn package** from the command line in order to create mcdiaspora.war file. This assumes you already have Apache Maven installed. 

Should you wish to skip this step and just run the application, grab the pre-built binary WAR file in the /binary folder above.

## Run mcdiaspora.war in Tomcat
This application was tested with Tomcat 9. Put the mcdisapora.war file in the webapp directory of Tomcat and fire it up.

## Call REST endpoint
With Tomcat 9 running, open a web browser and paste the URL below into the address bar to generate clusters:

    http://localhost:8080/mcdiaspora/cluster?name=Covid19

If you do not pass a cluster name, a default one will be used. Passing the same cluster name will result in the same cluster being returned (i.e. idempotent).

## Sample JSON returned
    {"clusterName":"COVID-19","systemList":[{"name":"SYSTEM_72833305","planetList":[{"name":"PLANET_62665133"},{"name":"PLANET_80108493"},{"name":"PLANET_75320495"},{"name":"PLANET_36354053"},{"name":"PLANET_92553599"},{"name":"PLANET_41295814"},{"name":"PLANET_60357267"},{"name":"PLANET_49321810"},{"name":"PLANET_39152263"},{"name":"PLANET_68963806"},{"name":"PLANET_47550648"},{"name":"PLANET_92257963"},{"name":"PLANET_16209800"},{"name":"PLANET_123206"}],"starList":[{"name":"STAR_94996628"}],"technologyAttribute":5,"environmentAtribute":6,"resourcesAttribute":1},{"name":"SYSTEM_7760852","planetList":[{"name":"PLANET_36071726"},{"name":"PLANET_69623819"},{"name":"PLANET_80733831"},{"name":"PLANET_34819949"},{"name":"PLANET_15270499"},{"name":"PLANET_98574739"},{"name":"PLANET_35609256"},{"name":"PLANET_80360421"},{"name":"PLANET_91188345"},{"name":"PLANET_63166931"},{"name":"PLANET_15728911"},{"name":"PLANET_70632139"},{"name":"PLANET_2557726"},{"name":"PLANET_20490188"},{"name":"PLANET_4174052"}],"starList":[],"technologyAttribute":5,"environmentAtribute":6,"resourcesAttribute":3}]}
