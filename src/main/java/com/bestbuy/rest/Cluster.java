package com.bestbuy.rest;

import java.util.List;

// Domain class
public class Cluster {

	String clusterName;
	List<System> systemList;
	
	public String getClusterName() {
		return clusterName;
	}
	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}
	public List<System> getSystemList() {
		return systemList;
	}
	public void setSystemList(List<System> systemList) {
		this.systemList = systemList;
	}
}
