package com.bestbuy.rest;

// Domain class
public class Planet {

	String name;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
