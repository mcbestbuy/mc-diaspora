package com.bestbuy.rest;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("")
public class MainApp extends ResourceConfig {
	public MainApp() {
		packages("com.bestbuy.rest");
	}
}
