package com.bestbuy.rest;

import java.util.List;

// Domain class
public class System {

	String name;
	List<Planet> planetList;
	List<Star> starList;
	Integer technologyAttribute;
	Integer environmentAtribute;
	Integer resourcesAttribute;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Planet> getPlanetList() {
		return planetList;
	}
	public void setPlanetList(List<Planet> planetList) {
		this.planetList = planetList;
	}
	public List<Star> getStarList() {
		return starList;
	}
	public void setStarList(List<Star> starList) {
		this.starList = starList;
	}
	public Integer getTechnologyAttribute() {
		return technologyAttribute;
	}
	public void setTechnologyAttribute(Integer technologyAttribute) {
		this.technologyAttribute = technologyAttribute;
	}
	public Integer getEnvironmentAtribute() {
		return environmentAtribute;
	}
	public void setEnvironmentAtribute(Integer environmentAtribute) {
		this.environmentAtribute = environmentAtribute;
	}
	public Integer getResourcesAttribute() {
		return resourcesAttribute;
	}
	public void setResourcesAttribute(Integer resourcesAttribute) {
		this.resourcesAttribute = resourcesAttribute;
	}	

}
