package com.bestbuy.rest;

// Domain class
public class Star {

	String name;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
