package com.bestbuy.rest;

import java.util.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("")
public class Controller {
	
	private static final Random random = new Random();
	private static final List<Cluster> clusterList = new ArrayList<>();   // our "database"
	
	// Just a quick sanity test, please ignore...
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/test")
	public System getTestSystem() {
		Planet planet = new Planet();
		planet.setName("Mike Chen");
		
		List<Planet> planetList = new ArrayList<>();
		planetList.add(planet);
		
		System testSystem = new System();
		testSystem.setName("Test System");
		testSystem.setPlanetList(planetList);
		
		return testSystem;
	}
	
	// Generates random cluster given a name
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cluster")	
	public Cluster generateCluster(@DefaultValue("default") @QueryParam("name") String name) {
		
		Cluster result = null;
		
		// If cluster already exists in "database", just return it.
		for (Cluster existingCluster : clusterList) {
			if (existingCluster.getClusterName().equalsIgnoreCase(name.trim())) {
				result = existingCluster;
				break;
			}
		}
		
		// Else if not found in "database", generate a new random cluster
		if (result == null) {
			int systemCount = random.nextInt(10) + 1;
			int starCount = random.nextInt(20) + 1;
			int planetCount = random.nextInt(30) + 1;
			
			List<System> systemList = new ArrayList<>();
			
			for (int i = 0; i < systemCount; i++) {
				System system = new System();
				system.setName("SYSTEM_" + random.nextInt(100000000));
				system.setStarList(new ArrayList<Star>());
				system.setPlanetList(new ArrayList<Planet>());
				system.setTechnologyAttribute(random.nextInt(6) + 1);
				system.setEnvironmentAtribute(random.nextInt(6) + 1);
				system.setResourcesAttribute(random.nextInt(6) + 1);
				system.setStarList(new ArrayList<Star>());
				system.setPlanetList(new ArrayList<Planet>());
				systemList.add(system);
			}
			
			for (int i = 0; i < starCount; i++) {
				Star star = new Star();
				star.setName("STAR_" + random.nextInt(100000000));
				System system = systemList.get(random.nextInt(systemCount));
				system.getStarList().add(star);
			}
			
			for (int i = 0; i < planetCount; i++) {
				Planet planet = new Planet();
				planet.setName("PLANET_" + random.nextInt(100000000));
				System system = systemList.get(random.nextInt(systemCount));
				system.getPlanetList().add(planet);
			}
			
			Cluster brandNewCluster = new Cluster();
			brandNewCluster.setClusterName(name.toUpperCase());
			brandNewCluster.setSystemList(systemList);
			clusterList.add(brandNewCluster); // add to "database"
			result = brandNewCluster;
		}
		
		return result;
	}
}
